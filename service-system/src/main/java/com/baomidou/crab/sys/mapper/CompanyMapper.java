package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Company;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统公司表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-10-21
 */
public interface CompanyMapper extends BaseMapper<Company> {

}
