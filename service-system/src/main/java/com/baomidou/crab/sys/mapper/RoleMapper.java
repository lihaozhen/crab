package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-09-15
 */
public interface RoleMapper extends BaseMapper<Role> {

}
